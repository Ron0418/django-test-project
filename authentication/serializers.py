from rest_framework import serializers
from .models import User
from rest_framework_simplejwt.tokens import RefreshToken

class LoginSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(max_length=255, min_length=2)
    password = serializers.CharField(max_length=65, min_length=8, write_only=True)
    name = serializers.SerializerMethodField()
    token = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['email', 'password','name','token']

    def get_name(self, data):
        email = data.get('email', '')
        password = data.get('password', '')
        user = User.objects.filter(email=email, password=password).first()
        if not user:
            raise serializers.ValidationError("Invalid credentials")
        return user.name
    def get_token(self, data):
        email = data.get('email', '')
        password = data.get('password', '')
        user = User.objects.filter(email=email, password=password).first()
        if not user:
            raise serializers.ValidationError("Invalid credentials")
        refresh = RefreshToken.for_user(user)
        token = {
            'refresh': str(refresh),
            'access': str(refresh.access_token),
        }
        return token
        


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','name','email']
        