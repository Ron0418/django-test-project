from django.urls import path
from .views import LoginView,UsersList


urlpatterns = [
    path('login/', LoginView.as_view()),
    path('users/', UsersList.as_view()),
]