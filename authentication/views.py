from .serializers import UserSerializer, LoginSerializer
from .models import User
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from rest_framework.pagination import PageNumberPagination
from rest_framework.generics import GenericAPIView,ListAPIView
# Create your views here.

class LoginView(GenericAPIView):
    """
    Return User Information With Access Token And Refresh Token
    """
    serializer_class = LoginSerializer

    def post(self, request):
        login_ser = self.get_serializer(data = request.data)
        if login_ser.is_valid():
            return Response(login_ser.data, status=status.HTTP_200_OK)
        else:
            return Response({"message": "failed", "details": login_ser.errors}, status=status.HTTP_400_BAD_REQUEST)

class UsersList(ListAPIView):
    """
    Return a list of all existing users.
    """
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class= PageNumberPagination
    def get_queryset(self):
        return User.objects.all()