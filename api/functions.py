from .models import CallRecord

def save_record(list,user):
    for status in list:
        if status == 'pending' or status == 'ready for approval' or status == 'resolved' or status == 'reopened':
            record = CallRecord.objects.create(
                user=user,
                call_type=status
            )
            record.save()