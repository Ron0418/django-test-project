from django.contrib import admin
from .models import ActionItem,Ticket,Customer,Note,CallRecord
# Register your models here.
admin.site.register(ActionItem)
admin.site.register(Ticket)
admin.site.register(Customer)
admin.site.register(Note)
admin.site.register(CallRecord)