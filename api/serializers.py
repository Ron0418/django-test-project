from rest_framework import serializers
from authentication.models import User
from .models import ActionItem,Ticket,Note,Customer,CallRecord
import datetime

class ActionItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActionItem
        fields = ['id','due_date','description','is_complete','rep','created_by']


class ActionItemCreateSerializer(serializers.ModelSerializer):
    rep_id = serializers.IntegerField()

    class Meta:
        model = ActionItem
        fields = [
            'id',
            'rep_id',
            'due_date',
            'description',
            'is_complete',
            'created_by'
        ]
        read_only_fields = ['id','created_by']

    def create(self,data):
        created_by = self.context["request"].user
        rep_id = data["rep_id"]
        due_date = data["due_date"]
        description = data["description"]

        try:
            is_complete = data["is_complete"]
        except:
            is_complete = False
        rep = User.objects.filter(id=rep_id).first()

        if not rep:
            raise serializers.ValidationError("rep does not exists")

        actionitem = ActionItem.objects.create(
            rep=rep,
            created_by=created_by,
            description=description,
            due_date=due_date,
            is_complete=is_complete
        )  
        actionitem.save()      
        return actionitem


class ActionItemUpdateSerializer(serializers.ModelSerializer):
    rep_id = serializers.SerializerMethodField()
    class Meta:
        model = ActionItem
        fields = [
            'id',
            'rep_id',
            'created_by',
            'description',
            'due_date',
            'is_complete'
        ]
        read_only_fields = [
            'rep_id',
            'created_by',
            'description',
            'due_date',
            'is_complete'
        ]

    def get_rep_id(self, obj):
        return obj.rep.id

    def update(self,actionitem,data):
        if not actionitem:
            raise serializers.ValidationError("actionitem does not exists")
        actionitem.is_complete = True
        actionitem.save()      
        return actionitem

class TicketSerializer(serializers.ModelSerializer):
    rep_id = serializers.SerializerMethodField()
    customer_id = serializers.SerializerMethodField()

    class Meta:
        model = Ticket
        fields = [
            'id',
            'customer_id',
            'contact_name',
            'contact_phone',
            'rep_id',
            'created_date',
            'resolved_date',
            'last_modified_date',
            'subject',
            'details',
            'status'
        ]
    
    def get_rep_id(self, obj):
        return obj.rep.id
    
    def get_customer_id(self, obj):
        return obj.customer.id

class NoteSerializer(serializers.ModelSerializer):
    rep_name = serializers.SerializerMethodField()
    rep_id = serializers.SerializerMethodField()

    class Meta:
        model = Note
        fields = [
            'id',
            'created_date',
            'rep_id',
            'rep_name',
            'note_text'
        ]

    def get_rep_id(self, obj):
        return obj.rep.id

    def get_rep_name(self, obj):
        return obj.rep.name

class TicketRetrieveSerializer(serializers.ModelSerializer):
    rep_name = serializers.SerializerMethodField()
    customer_id = serializers.SerializerMethodField()
    rep_id = serializers.SerializerMethodField()
    notes = NoteSerializer(many=True)

    class Meta:
        model = Ticket
        fields = [
            'id',
            'customer_id',
            'contact_name',
            'contact_phone',
            'rep_id',
            'rep_name',
            'subject',
            'details',
            'created_date',
            'resolved_date',
            'status',
            'notes'
        ]

    def get_customer_id(self, obj):
        return obj.customer.id

    def get_rep_id(self, obj):
        return obj.rep.id

    def get_rep_name(self, obj):
        return obj.rep.name


class CustomerRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ['id','full_name','phone']


class TicketCreateSerializer(serializers.ModelSerializer):
    rep_id = serializers.SerializerMethodField()
    customer_id = serializers.IntegerField()

    class Meta:
        model = Ticket
        fields = [
            'id',
            'customer_id',
            'contact_name',
            'contact_phone',
            'rep_id',
            'subject',
            'details',
            'created_date',
            'resolved_date',
            'last_modified_date',
            'status'
        ]
        read_only_fields = [
            'id',
            'rep_id',
            'created_date',
            'resolved_date',
            'last_modified_date',
            'status'
        ]

    def create(self,data):
        created_by = self.context["request"].user
        customer_id = data["customer_id"]
        contact_name = data["contact_name"]
        contact_phone = data["contact_phone"]
        subject = data["subject"]
        details = data["details"]
        customer = Customer.objects.filter(id=customer_id).first()
        if not customer:
            raise serializers.ValidationError("customer does not exists")
        ticket = Ticket.objects.create(
            customer=customer,
            contact_name=contact_name,
            contact_phone=contact_phone,
            subject=subject,
            details=details,
            rep=created_by
        )  
        ticket.save()      
        return ticket

    def get_rep_id(self, obj):
        return obj.rep.id

class TicketUpdateSerializer(serializers.ModelSerializer):
    customer_id = serializers.SerializerMethodField()
    rep_id = serializers.SerializerMethodField()

    class Meta:
        model = Ticket
        fields = [
            'id',
            'customer_id',
            'contact_name',
            'contact_phone',
            'rep_id',
            'subject',
            'details',
            'created_date',
            'resolved_date',
            'last_modified_date',
            'status'
        ]
        read_only_fields = [
            'customer_id',
            'contact_name',
            'contact_phone',
            'rep_id',
            'subject',
            'details',
            'created_date',
            'resolved_date',
            'last_modified_date'
        ]

    def get_customer_id(self, obj):
        return obj.customer.id

    def get_rep_id(self, obj):
        return obj.rep.id

    def update(self,ticket,data):
        if not ticket:
            raise serializers.ValidationError("ticket does not exists")
        try:
            status = data["status"]
        except:
            raise serializers.ValidationError("status field required")
        ticket.status = status
        ticket.last_modified_date = datetime.datetime.now()
        ticket.save()      
        return ticket

class NoteCreateSerializer(serializers.ModelSerializer):
    rep_id = serializers.SerializerMethodField()
    ticket_id = serializers.IntegerField()
    class Meta:
        model = Note
        fields = ['id','ticket_id','note_text','rep_id','created_date']
        read_only_fields = ['id','created_date','rep_id']

    def create(self,data):
        created_by = self.context["request"].user
        ticket_id = data["ticket_id"]
        note_text = data["note_text"]
        ticket = Ticket.objects.filter(id=ticket_id).first()
        if not ticket:
            raise serializers.ValidationError("ticket does not exists")
        note = Note.objects.create(
            ticket=ticket,
            note_text=note_text,
            rep=created_by
        )  
        note.save()      
        return note
    def get_rep_id(self, obj):
        return obj.rep.id



# CallRecord
class RecordSerializer(serializers.Serializer):
    pending = serializers.SerializerMethodField()
    ready_for_approval = serializers.SerializerMethodField()
    reopened = serializers.SerializerMethodField()
    resolved = serializers.SerializerMethodField()
    def get_pending(self, request):
        user = self.context.get('request').user
        count = CallRecord.objects.filter(user=user,call_type="pending").count()
        return count
    def get_ready_for_approval(self, request):
        user = self.context.get('request').user
        count = CallRecord.objects.filter(user=user,call_type="ready for approval").count()
        return count
    def get_reopened(self, request):
        user = self.context.get('request').user
        count = CallRecord.objects.filter(user=user,call_type="reopened").count()
        return count
    def get_resolved(self, request):
        user = self.context.get('request').user
        count = CallRecord.objects.filter(user=user,call_type="resolved").count()
        return count

    