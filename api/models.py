from django.db import models
from authentication.models import User
from django.utils import timezone

# Create your models here.
STATUS_CATEGORY = (
    ('pending','pending'),
    ('ready for approval','ready for approval'),
    ('resolved','resolved'),
    ('reopened','reopened')
)

class Customer(models.Model):
    full_name = models.CharField(max_length=256)
    phone = models.IntegerField()
    def __str__(self):
        return str(self.pk)

class Ticket(models.Model):
    customer = models.ForeignKey(Customer,on_delete=models.CASCADE)
    contact_name = models.CharField(max_length=256)
    contact_phone = models.IntegerField()
    rep = models.ForeignKey(User,on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=timezone.now)
    resolved_date = models.DateTimeField(null=True)
    last_modified_date = models.DateTimeField(default=timezone.now)
    subject = models.CharField(max_length=256)
    details = models.CharField(max_length=256)
    status = models.CharField(choices=STATUS_CATEGORY,default="pending",max_length=256)
    def __str__(self):
        return str(self.pk)

class Note(models.Model):
    ticket = models.ForeignKey(Ticket,on_delete=models.CASCADE,related_name="notes")
    rep = models.ForeignKey(User,on_delete=models.CASCADE)
    note_text = models.CharField(max_length=256)
    created_date = models.DateTimeField(default=timezone.now)
    def __str__(self):
        return str(self.pk)

class ActionItem(models.Model):
    rep = models.ForeignKey(User,on_delete=models.CASCADE,related_name='rep_id')
    created_by = models.ForeignKey(User,on_delete=models.CASCADE,related_name='created_by')
    description = models.CharField(max_length=256)
    due_date = models.DateTimeField()
    is_complete = models.BooleanField(default=False)
    def __str__(self):
        return str(self.pk)

class CallRecord(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE,related_name='call_user')
    call_type = models.CharField(choices=STATUS_CATEGORY,max_length=256)
    def __str__(self):
        return str(self.pk)

         