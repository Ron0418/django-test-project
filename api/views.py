from .models import ActionItem,Ticket,Note,Customer
from .serializers import (
    ActionItemSerializer,
    ActionItemCreateSerializer,
    ActionItemUpdateSerializer,
    TicketSerializer,
    TicketRetrieveSerializer,
    CustomerRetrieveSerializer,
    TicketCreateSerializer,
    TicketUpdateSerializer,
    NoteCreateSerializer,
    RecordSerializer
)
from rest_framework.pagination import PageNumberPagination
from rest_framework import permissions
from .functions import save_record
from rest_framework.generics import ListAPIView,CreateAPIView,UpdateAPIView,RetrieveAPIView
from rest_framework import generics,status
from rest_framework.response import Response
# Create your views here.

class ActionItemsList(ListAPIView):
    """
    Get Action Items For Logged In User Optionally Can Be Filtered 
    By is_complete true or false
    """
    serializer_class = ActionItemSerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class= PageNumberPagination
    queryset = ActionItem.objects.all()

    def get_queryset(self):
        queryset=ActionItem.objects.filter(created_by=self.request.user)
        is_complete = self.request.query_params.get('is_complete', '')
        if is_complete == 'true':
            queryset=queryset.filter(created_by=self.request.user,is_complete=True)
        if is_complete == 'false':
            queryset=queryset.filter(created_by=self.request.user,is_complete=False)
        return queryset

class ActionItemsCreateAPI(CreateAPIView):
    """
    Create Action Item 
    """
    serializer_class = ActionItemCreateSerializer
    permission_classes = (permissions.IsAuthenticated,)
    queryset = ActionItem.objects.all() 


class ActionItemsUpdateAPI(UpdateAPIView):
    """
    Resolve Action Item It Takes ActionItem Id and It Will 
    Set is_complete Field To true
    """
    queryset = ActionItem.objects.all()
    serializer_class = ActionItemUpdateSerializer
    permission_classes = (permissions.IsAuthenticated,)
    lookup_field = 'id'


class TicketList(ListAPIView):
    """
    Get All Tickers Van Be Filterd By Date Range and Status Field
    Paramaters Are start_date and end_date and status_list 
    """
    serializer_class = TicketSerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class= PageNumberPagination

    def get_queryset(self):
        start_date = self.request.query_params.get('start_date', '')
        end_date = self.request.query_params.get('end_date', '')
        status_list = self.request.query_params.get('status_list', '')
        queryset= Ticket.objects.all()
        if start_date:
            queryset=queryset.filter(created_date__gte=start_date)
        if end_date:
            queryset=queryset.filter(created_date__lte=end_date)
        if status_list:
            list = status_list.split(",")
            queryset=queryset.filter(status__in=list)
            save_record(list,self.request.user)
        return queryset
       

class TicketRetrieveAPI(RetrieveAPIView):
    """
    Takes Ticker Id And Get Ticker Detail
    """
    queryset = Ticket.objects.all()
    serializer_class = TicketRetrieveSerializer
    permission_classes = (permissions.IsAuthenticated,)
    lookup_field = 'id'


class CustomerRetrieveAPI(RetrieveAPIView):
    """
    Takes Customer Id And Get Customer Detail
    """
    queryset = Customer.objects.all()
    serializer_class = CustomerRetrieveSerializer
    permission_classes = (permissions.IsAuthenticated,)
    lookup_field = 'id'

class TicketCreateAPI(CreateAPIView):
    """
    Create Ticker Item
    """
    serializer_class = TicketCreateSerializer
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Ticket.objects.all()

class TicketUpdateAPI(UpdateAPIView):
    """
    Takes Ticker Id And status Field And Update The Ticker With The New status
    """
    queryset = Ticket.objects.all()
    serializer_class = TicketUpdateSerializer
    permission_classes = (permissions.IsAuthenticated,)
    lookup_field = 'id'


class NoteCreateAPI(CreateAPIView):
    """
    Create Note Item
    """
    queryset = Note.objects.all()
    serializer_class = NoteCreateSerializer
    permission_classes = (permissions.IsAuthenticated,)

class RecordList(generics.GenericAPIView):
    """
    Get Call Summary Of The User Request Status Types
    """
    serializer_class = RecordSerializer
    permission_classes = (permissions.IsAuthenticated,)
    def get(self,request):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        if serializer.is_valid():
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
