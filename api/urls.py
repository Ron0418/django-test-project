from django.urls import path
from .views import (
    ActionItemsList,
    ActionItemsCreateAPI,
    ActionItemsUpdateAPI,
    TicketList,
    TicketRetrieveAPI,
    CustomerRetrieveAPI,
    TicketCreateAPI,
    TicketUpdateAPI,
    NoteCreateAPI,
    RecordList
)

urlpatterns = [
    path('actionitems/', ActionItemsList.as_view()),
    path('actionitems/create/', ActionItemsCreateAPI.as_view()),
    path('actionitems/resolve/<int:id>/', ActionItemsUpdateAPI.as_view()),
    path('tickets/', TicketList.as_view()),
    path('tickets/create/', TicketCreateAPI.as_view()),
    path('tickets/<int:id>/', TicketRetrieveAPI.as_view()),
    path('tickets/update/<int:id>/', TicketUpdateAPI.as_view()),
    path('customers/<int:id>/', CustomerRetrieveAPI.as_view()),
    path('notes/create/', NoteCreateAPI.as_view()),
    path('callsummary/', RecordList.as_view()),
]